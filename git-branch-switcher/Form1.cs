﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using z.Console;

namespace git_branch_switcher
{
    public partial class Form1 : Form
    {
        private string CWD;
        private const string Git = "git.exe";

        private List<Branch> Branches = new List<Branch>();

        public Form1()
        {
            InitializeComponent();
            this.Text = $"{ Application.ProductName} [{ Application.ProductVersion }]";
          
            CWD = Directory.GetCurrentDirectory();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitiateDirectory();
        }

        private async void Button1_Click(object sender, EventArgs e)
        {
            PrintOutput("User: Update list of Branches");
            await Task.Run(() =>
            {
                PrintOutput(OneLiner("remote update"));
            }).ContinueWith(t => ListBranch());
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            SwitchBranch();
        }

        #region "Methods"

        private void InitiateDirectory()
        {
            bool isGitDirectory;
            if (Boolean.TryParse(OneLiner("rev-parse --is-inside-work-tree"), out isGitDirectory))
            {
                PrintOutput("Valid git repository");
            }
            else
            {
                PrintOutput("Not a git directory");
                PrintOutput("Exiting");

                if (!Debugger.IsAttached)
                {
                    MessageBox.Show("Not a git directory.", this.Text);
                    Application.Exit();
                }
            }

            lblOrigin.Text = "Fetching Master branch ...";
            GetMasterBranch().ContinueWith(x => Invoker(() => lblOrigin.Text = x.Result));
            ListBranch();
        }

        private async void ListBranch(bool Output = true)
        {
            if (Output)
                PrintOutput("User: Listing Branches");

            await Task.Run(() =>
            {
                var branches = OneLiner("branch -a");

                Branches.Clear();
                Branches.AddRange(branches.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Select((x, i) =>
                    new Branch
                    {
                        Name = x.Replace("*", "").Trim(),
                        IsRemote = x.Trim().StartsWith("remotes"),
                        IsDefault = x.Trim().StartsWith("*"),
                        Index = i
                    }));

                Invoker(() =>
                {
                    txtWorkingBranch.Text = Branches.Single(x => x.IsDefault).Name;
                    cbBranch.DataSource = null;
                    cbBranch.ValueMember = "Name";
                    cbBranch.DisplayMember = "Name";
                    cbBranch.DataSource = Branches;
                    cbBranch.SelectedValue = txtWorkingBranch.Text;
                    cbBranch.Refresh();
                });

                if (Output)
                    PrintOutput(branches);
            });
        }

        private async void SwitchBranch()
        {
            if (txtWorkingBranch.Text == cbBranch.SelectedValue.ToString())
            {
                PrintOutput("Warning: Select another branch");
                return;
            }

            this.btnSwitch.Text = "Switching";
            EnableControls(false);

            var stash = chkStashChanges.Checked;
            var branchTo = cbBranch.SelectedValue.ToString();
            var isRemoteBranch = Branches.Single(x => x.Name == branchTo).IsRemote;
            var isReplicatedInLocal = IsBranchReplicated(branchTo);
            var master = lblOrigin.Text;
            var pullOrigin = chkPullOrigin.Checked;

            await Task.Run(() =>
            {
                PrintOutput(OneLiner("status"));

                if (stash)
                {
                    //Stash
                    PrintOutput("User: Stashing Changes");
                    PrintOutput(OneLiner("stash"));
                }
                else
                {
                    //reset
                    PrintOutput(OneLiner("reset --hard"));
                }

                PrintOutput("User: Switching Branch");
                if (isReplicatedInLocal)
                    PrintOutput(OneLiner($"checkout { branchTo }"));
                else
                {
                    PrintOutput(OneLiner($"checkout -b { LocalizeBranchName(branchTo) }"));
                }

                if (stash)
                {
                    PrintOutput(OneLiner("stash apply"));
                    PrintOutput(OneLiner("stash clear"));
                }

                if (pullOrigin)
                {
                    PrintOutput("User: Pulling Origin Branch");
                    PrintOutput(OneLiner($"pull origin {master}"));
                }

                ListBranch(false);
                PrintOutput(OneLiner("status"));
                PrintOutput("-- Switching Branch Completed --");
            }).ContinueWith(x => Invoker(() =>
            {
                this.btnSwitch.Text = "Switch";
                EnableControls(true);
            }));
        }

        private bool IsBranchReplicated(string branch)
        {
            var curBranch = Branches.Single(x => x.Name == branch);

            if (!curBranch.IsRemote)
                return true;

            return Branches.Any(x => x.Index != curBranch.Index && CleanName(x.Name) == CleanName(branch) && !x.IsRemote);
        }

        private void PrintOutput(string message)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate { PrintOutput(message); }));
                return;
            }

            txtOutput.AppendText($"{message}\r\n");
            txtOutput.ScrollToCaret();
        }

        private string OneLiner(string command)
        {
            using (var app = new ConsoleApp(Git, command))
            {
                var outputStringBuilder = new StringBuilder();
                app.ConsoleOutput += (o, args) => outputStringBuilder.AppendLine(args.Line);
                app.WorkingDirectory = CWD;

                app.Run();
                app.WaitForExit();

                return outputStringBuilder.ToString();
            }
        }

        private void Invoker(Action action) => this.BeginInvoke(new MethodInvoker(delegate { action(); }));

        private string CleanName(string Name)
        {
            return Name.Substring(Math.Abs(Name.LastIndexOf('/')) - 1);
        }

        private async Task<string> GetMasterBranch()
        {
            return await Task.Run<string>(() =>
            {
                var lsd = OneLiner("remote show origin").Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                return lsd.Single(x => x.Trim().StartsWith("HEAD branch")).Split(':')[1].Trim();
            });
        }

        private void EnableControls(bool Enable)
        {
            this.cbBranch.Enabled = btnRemoteUpdate.Enabled = chkPullOrigin.Enabled = chkStashChanges.Enabled = btnSwitch.Enabled = btnCancel.Enabled = Enable;
        }

        private string LocalizeBranchName(string Name)
        {
            var str = new string[]
            {
                "remotes/",
                "origin/",
                "feature/"
            };

            var newName = Name;

            foreach (var s in str)
            {
                newName = newName.TrimStart(s.ToCharArray());
            }

            return newName;
        }

        #endregion

        private void Button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                var absouluteFile = System.Reflection.Assembly.GetExecutingAssembly().Location;

                var str = new StringBuilder();
                str.AppendLine("Windows Registry Editor Version 5.00");
                str.AppendLine();
                str.AppendLine(@"[HKEY_CLASSES_ROOT\Directory\shell\git_switcher]");
                str.AppendLine(@"@=""Git &Switch Here""");
                str.AppendLine($@"""Icon""=""{ absouluteFile.Replace("\\", @"\\") }""");
                str.AppendLine();
                str.AppendLine(@"[HKEY_CLASSES_ROOT\Directory\shell\git_switcher\command]");
                str.AppendLine($@"@=""\""{ absouluteFile.Replace("\\", @"\\") }\"" \""--working-dir\"" \""%1\""""");
                str.AppendLine();
                str.AppendLine(@"[HKEY_CLASSES_ROOT\Directory\Background\shell\git_switcher]");
                str.AppendLine(@"@=""Git &Switch Here""");
                str.AppendLine($@"""Icon""=""{ absouluteFile.Replace("\\", @"\\") }""");
                str.AppendLine();
                str.AppendLine(@"[HKEY_CLASSES_ROOT\Directory\Background\shell\git_switcher\command]");
                str.AppendLine($@"@=""\""{ absouluteFile.Replace("\\", @"\\") }\"" \""--working-dir\"" \""%v.\""""");

                var regFile = Path.Combine(Path.GetDirectoryName(absouluteFile), "git-switcher.reg");

                File.WriteAllText(regFile, str.ToString());

                Process.Start(regFile); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }

    class Branch
    {
        public string Name { get; set; }
        public bool IsRemote { get; set; }
        public bool IsDefault { get; set; }
        public int Index { get; set; }
    }
}
