﻿namespace git_branch_switcher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.cbBranch = new System.Windows.Forms.ComboBox();
            this.chkStashChanges = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnRemoteUpdate = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSwitch = new System.Windows.Forms.Button();
            this.txtWorkingBranch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkPullOrigin = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOrigin = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtOutput
            // 
            this.txtOutput.Location = new System.Drawing.Point(12, 188);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(635, 176);
            this.txtOutput.TabIndex = 0;
            // 
            // cbBranch
            // 
            this.cbBranch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBranch.FormattingEnabled = true;
            this.cbBranch.Location = new System.Drawing.Point(25, 114);
            this.cbBranch.Name = "cbBranch";
            this.cbBranch.Size = new System.Drawing.Size(493, 21);
            this.cbBranch.TabIndex = 1;
            // 
            // chkStashChanges
            // 
            this.chkStashChanges.AutoSize = true;
            this.chkStashChanges.Checked = true;
            this.chkStashChanges.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkStashChanges.Location = new System.Drawing.Point(52, 152);
            this.chkStashChanges.Name = "chkStashChanges";
            this.chkStashChanges.Size = new System.Drawing.Size(98, 17);
            this.chkStashChanges.TabIndex = 2;
            this.chkStashChanges.Text = "Stash Changes";
            this.chkStashChanges.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Working Branch";
            // 
            // btnRemoteUpdate
            // 
            this.btnRemoteUpdate.Location = new System.Drawing.Point(525, 113);
            this.btnRemoteUpdate.Name = "btnRemoteUpdate";
            this.btnRemoteUpdate.Size = new System.Drawing.Size(122, 23);
            this.btnRemoteUpdate.TabIndex = 4;
            this.btnRemoteUpdate.Text = "Remote Update";
            this.btnRemoteUpdate.UseVisualStyleBackColor = true;
            this.btnRemoteUpdate.Click += new System.EventHandler(this.Button1_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(557, 380);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.Button2_Click);
            // 
            // btnSwitch
            // 
            this.btnSwitch.Location = new System.Drawing.Point(462, 380);
            this.btnSwitch.Name = "btnSwitch";
            this.btnSwitch.Size = new System.Drawing.Size(75, 23);
            this.btnSwitch.TabIndex = 6;
            this.btnSwitch.Text = "Switch";
            this.btnSwitch.UseVisualStyleBackColor = true;
            this.btnSwitch.Click += new System.EventHandler(this.Button3_Click);
            // 
            // txtWorkingBranch
            // 
            this.txtWorkingBranch.Location = new System.Drawing.Point(25, 29);
            this.txtWorkingBranch.Name = "txtWorkingBranch";
            this.txtWorkingBranch.ReadOnly = true;
            this.txtWorkingBranch.Size = new System.Drawing.Size(621, 20);
            this.txtWorkingBranch.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Branch To";
            // 
            // chkPullOrigin
            // 
            this.chkPullOrigin.AutoSize = true;
            this.chkPullOrigin.Checked = true;
            this.chkPullOrigin.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkPullOrigin.Location = new System.Drawing.Point(183, 152);
            this.chkPullOrigin.Name = "chkPullOrigin";
            this.chkPullOrigin.Size = new System.Drawing.Size(73, 17);
            this.chkPullOrigin.TabIndex = 9;
            this.chkPullOrigin.Text = "Pull Origin";
            this.chkPullOrigin.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Origin:";
            // 
            // lblOrigin
            // 
            this.lblOrigin.AutoSize = true;
            this.lblOrigin.Location = new System.Drawing.Point(93, 53);
            this.lblOrigin.Name = "lblOrigin";
            this.lblOrigin.Size = new System.Drawing.Size(38, 13);
            this.lblOrigin.TabIndex = 11;
            this.lblOrigin.Text = "master";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 380);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Register in Context Menu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 423);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblOrigin);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.chkPullOrigin);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtWorkingBranch);
            this.Controls.Add(this.btnSwitch);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRemoteUpdate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkStashChanges);
            this.Controls.Add(this.cbBranch);
            this.Controls.Add(this.txtOutput);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.ComboBox cbBranch;
        private System.Windows.Forms.CheckBox chkStashChanges;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRemoteUpdate;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSwitch;
        private System.Windows.Forms.TextBox txtWorkingBranch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkPullOrigin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblOrigin;
        private System.Windows.Forms.Button button1;
    }
}

